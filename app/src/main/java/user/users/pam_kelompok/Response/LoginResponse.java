package user.users.pam_kelompok.Response;

public class LoginResponse {

    String status;
    int role;
    int id_user;

    public LoginResponse(String status, int role, int id_user) {
        this.status = status;
        this.role = role;
        this.id_user = id_user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "status='" + status + '\'' +
                ", role=" + role +
                ", id_user=" + id_user +
                '}';
    }
}
