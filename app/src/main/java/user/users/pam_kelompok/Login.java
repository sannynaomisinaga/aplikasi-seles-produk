package user.users.pam_kelompok;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.PemasokBarang.Index_PBarang;
import user.users.pam_kelompok.PemilikWarung.IndexPemilikWarung;
import user.users.pam_kelompok.Response.LoginResponse;

public class Login extends AppCompatActivity {

    ApiInterface apiInterface;
    EditText usrname,pssword;
    Button buttn_login;
    Button btn_reg;

    static int id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usrname = (EditText) findViewById(R.id.tf_username);
        pssword = (EditText) findViewById(R.id.tf_password);

        buttn_login = (Button) findViewById(R.id.btn_login);
        btn_reg = (Button) findViewById(R.id.btn_regist);

        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(Login.this, Register.class);
//                x.putExtra("id_user", id_user);
                startActivity(x);
            }
        });
        buttn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cekLogin();
            }
        });

    }

    public void cekLogin() {
        String username = usrname.getText().toString();
        String password = pssword.getText().toString();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<LoginResponse> call = apiInterface.userLogin(username, password);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                LoginResponse loginResponse = response.body();

                System.out.println(loginResponse);

                if(loginResponse.getStatus().equalsIgnoreCase("success")) {

                    id_user = loginResponse.getId_user();

                    if(loginResponse.getRole() == 1) {

                        System.out.println("Pemasok Barang berhasil masuk");
                        Intent intent = new Intent(Login.this, Index_PBarang.class);
                        intent.putExtra("id_user", id_user);
                        startActivity(intent);

                    } else if(loginResponse.getRole() == 2) {
                        System.out.println("Pemasok Barang berhasil masuk");
                        Intent intent = new Intent(Login.this, IndexPemilikWarung.class);
                        intent.putExtra("id_user", id_user);
                        startActivity(intent);
                    }
                } else {
                    System.out.println("Terjadi Kesalahan");
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }

}
