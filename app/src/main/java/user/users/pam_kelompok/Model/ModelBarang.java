package user.users.pam_kelompok.Model;

public class ModelBarang {

    int id;
    int id_pemasokBarang;
    String nama_barang;
    Double harga_barang;
    String nomor_barang, deskripsi_barang, gambar, status;

    public ModelBarang() {}

    public ModelBarang(String nama_barang, Double harga_barang, String deskripsi_barang, String status) {
        this.nama_barang = nama_barang;
        this.harga_barang = harga_barang;
        this.deskripsi_barang = deskripsi_barang;
        this.status = status;
    }

    public ModelBarang(int id_pemasokBarang, String nama_barang, Double harga_barang, String nomor_barang, String deskripsi_barang, String gambar, String status) {
        this.id_pemasokBarang = id_pemasokBarang;
        this.nama_barang = nama_barang;
        this.harga_barang = harga_barang;
        this.nomor_barang = nomor_barang;
        this.deskripsi_barang = deskripsi_barang;
        this.gambar = gambar;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_pemasokBarang() {
        return id_pemasokBarang;
    }

    public void setId_pemasokBarang(int id_pemasokBarang) {
        this.id_pemasokBarang = id_pemasokBarang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public Double getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(Double harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getNomor_barang() {
        return nomor_barang;
    }

    public void setNomor_barang(String nomor_barang) {
        this.nomor_barang = nomor_barang;
    }

    public String getDeskripsi_barang() {
        return deskripsi_barang;
    }

    public void setDeskripsi_barang(String deskripsi_barang) {
        this.deskripsi_barang = deskripsi_barang;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
