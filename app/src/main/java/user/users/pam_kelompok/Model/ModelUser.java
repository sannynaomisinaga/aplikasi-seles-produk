package user.users.pam_kelompok.Model;

public class ModelUser {
    int id;
    String username,password,nama,alamat,no_ktp,no_telp,deskripsi;
    int role;

    @Override
    public String toString() {
        return "ModelUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nama='" + nama + '\'' +
                ", alamat='" + alamat + '\'' +
                ", no_ktp='" + no_ktp + '\'' +
                ", no_telp='" + no_telp + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", role=" + role +
                '}';
    }

    public ModelUser(String username, String password, String nama, String alamat, String no_ktp, String no_telp, String deskripsi, int role) {
        this.username = username;
        this.password = password;
        this.nama = nama;
        this.alamat = alamat;
        this.no_ktp = no_ktp;
        this.no_telp = no_telp;
        this.deskripsi = deskripsi;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_ktp() {
        return no_ktp;
    }

    public void setNo_ktp(String no_ktp) {
        this.no_ktp = no_ktp;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
