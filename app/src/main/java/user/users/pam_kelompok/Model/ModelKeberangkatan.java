package user.users.pam_kelompok.Model;

public class ModelKeberangkatan {
    int id;
    int id_pemasokBarang;
    String nama_pemasokBarang,tujuan,tanggal;


    public ModelKeberangkatan() {}
    public ModelKeberangkatan(int id_pemasokBarang, String tujuan, String tanggal) {
        this.id_pemasokBarang = id_pemasokBarang;
        this.tujuan = tujuan;
        this.tanggal = tanggal;
    }

    public ModelKeberangkatan(int id_pemasokBarang, String nama_pemasokBarang, String tujuan, String tanggal) {
        this.id_pemasokBarang = id_pemasokBarang;
        this.nama_pemasokBarang = nama_pemasokBarang;
        this.tujuan = tujuan;
        this.tanggal = tanggal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_pemasokBarang() {
        return id_pemasokBarang;
    }

    public void setId_pemasokBarang(int id_pemasokBarang) {
        this.id_pemasokBarang = id_pemasokBarang;
    }

    public String getNama_pemasokBarang() {
        return nama_pemasokBarang;
    }

    public void setNama_pemasokBarang(String nama_pemasokBarang) {
        this.nama_pemasokBarang = nama_pemasokBarang;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
