package user.users.pam_kelompok;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.Model.ModelUser;

public class Register extends AppCompatActivity {

    EditText username,password,nama,alamat,no_ktp,no_telp,deskripsi,role;
    String username1,password1,nama1,alamat1,no_ktp1,no_telp1,deskripsi1,role1;

    Button btn_daftar;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = (EditText) findViewById(R.id.tf_username);
        password = (EditText) findViewById(R.id.tf_password);
        alamat = (EditText) findViewById(R.id.tf_alamat);
        no_ktp = (EditText) findViewById(R.id.tf_no_ktp);
        no_telp = (EditText) findViewById(R.id.tf_no_telp);
        nama = (EditText) findViewById(R.id.tf_nama);
        deskripsi = (EditText) findViewById(R.id.tf_deskripsi);
        role = (EditText) findViewById(R.id.tf_role);

        btn_daftar = (Button) findViewById(R.id.btn_register);

        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });
    }

    public void createUser() {

        username1 = username.getText().toString();
        password1 = password.getText().toString();
        alamat1 = alamat.getText().toString();
        no_ktp1 = no_ktp.getText().toString();
        no_telp1 = no_telp.getText().toString();
        nama1 = nama.getText().toString();
        deskripsi1 = deskripsi.getText().toString();
        role1 = role.getText().toString();
        int rolee = Integer.parseInt(role1);

//        ModelUser modelUser = new ModelUser(username1,password1,nama1,alamat1,no_ktp1,no_telp1,deskripsi1,rolee);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ModelUser> call = apiInterface.registerAkun(username1,password1,nama1,alamat1,no_ktp1,no_telp1,deskripsi1,rolee);

        call.enqueue(new Callback<ModelUser>() {
            @Override
            public void onResponse(Call<ModelUser> call, Response<ModelUser> response) {
                if(response.isSuccessful()) {
                    System.out.println("Berhasil Registrasi");

                    Intent intent = new Intent(Register.this, Login.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ModelUser> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }
}
