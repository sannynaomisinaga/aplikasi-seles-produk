package user.users.pam_kelompok.API;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import user.users.pam_kelompok.Model.ModelBarang;
import user.users.pam_kelompok.Model.ModelKeberangkatan;
import user.users.pam_kelompok.Model.ModelUser;
import user.users.pam_kelompok.Response.LoginResponse;

public interface ApiInterface {



    @FormUrlEncoded
    @POST("tambahKeberangkatan")
    Call<ModelKeberangkatan> tambahKeberangkatan (
            @Field("id_pemasokBarang") int id_pemasokBarang,
            @Field("tujuan") String tujuan,
            @Field("tanggal") String tanggal
    );

    //Register Akun
    @FormUrlEncoded
    @POST("registerAkun")
    Call<ModelUser> registerAkun (
        @Field("username") String username,
        @Field("password") String password,
        @Field("nama") String nama,
        @Field("alamat") String alamat,
        @Field("no_ktp") String no_ktp,
        @Field("no_telp") String no_telp,
        @Field("deskripsi") String deskripsi,
        @Field("role") int role
    );

    @FormUrlEncoded
    @POST("cekLogin")
    Call<LoginResponse> userLogin(
        @Field("username") String username,
        @Field("password") String password
    );

    @FormUrlEncoded
    @POST("tambahBarang")
    Call<ModelBarang> registerBarang (
            @Field("id_pemasokBarang") int id_pemasokBarang,
            @Field("nama_barang") String nama_barang,
            @Field("harga_barang") Double harga_barang,
            @Field("nomor_barang") String nomor_barang,
            @Field("deskripsi_barang") String deskripsi_barang,
            @Field("gambar") String gambar,
            @Field("status") String status
    );

    @GET("barangPerPemasok/{id}")
    Call<List<ModelBarang>> getBarangPerPBarang(@Path("id") int id_pemasokBarang);

    @GET("getAllKeberangkatan")
    Call<List<ModelKeberangkatan>> getAllKeberangkatan();


    @DELETE("deleteBarang/{id}")
    Call<Void> deleteBarang(@Path("id") int id);

    @PUT("updateBarang/{id}")
    Call<ModelBarang> updateBarang(@Path("id") int id, @Body ModelBarang modelBarang);
}
