package user.users.pam_kelompok.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


import user.users.pam_kelompok.Model.ModelBarang;
import user.users.pam_kelompok.PemasokBarang.DetailBarang;
import user.users.pam_kelompok.R;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.BarangViewHolder> {

    private Context mContext;
    private List<ModelBarang> modelBarangs;

    public BarangAdapter(Context mContext, List<ModelBarang> modelBarangs) {
        this.mContext = mContext;
        this.modelBarangs = modelBarangs;
    }

    @NonNull
    @Override
    public BarangViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;

            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(R.layout.card_view_barang, viewGroup, false);
        return new BarangViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BarangViewHolder barangViewHolder, final int i) {
        final ModelBarang modelBarang = modelBarangs.get(i);

        barangViewHolder.namaBarang.setText(modelBarang.getNama_barang());

        barangViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailBarang.class);

                intent.putExtra("id_user", modelBarang.getId());
                intent.putExtra("id_user1", modelBarang.getId_pemasokBarang());
                intent.putExtra("nama", modelBarang.getNama_barang());
                intent.putExtra("harga", modelBarang.getHarga_barang());
                intent.putExtra("desc", modelBarang.getDeskripsi_barang());
                intent.putExtra("stat", modelBarang.getStatus());

                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
       return modelBarangs.size();
    }

    public class BarangViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView namaBarang;

        public BarangViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.id_cardview);
            namaBarang = (TextView) itemView.findViewById(R.id.nama_barang);
        }
    }
}
