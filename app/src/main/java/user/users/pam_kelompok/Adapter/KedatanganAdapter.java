package user.users.pam_kelompok.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import user.users.pam_kelompok.Model.ModelKeberangkatan;
import user.users.pam_kelompok.PemilikWarung.DetailPemasokBarang;
import user.users.pam_kelompok.R;

public class KedatanganAdapter extends RecyclerView.Adapter<KedatanganAdapter.KedatanganViewHolder> {

    private Context mContext;
    private List<ModelKeberangkatan> modelKeberangkatans;

    public KedatanganAdapter(Context mContext, List<ModelKeberangkatan> modelKeberangkatans) {
        this.mContext = mContext;
        this.modelKeberangkatans = modelKeberangkatans;
    }

    @NonNull
    @Override
    public KedatanganViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        view = layoutInflater.inflate(R.layout.item_list_kedatangan_barang, viewGroup, false);

        return new KedatanganViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KedatanganViewHolder kedatanganViewHolder, int i) {
        final ModelKeberangkatan modelKeberangkatan = modelKeberangkatans.get(i);

        kedatanganViewHolder.a.setText(modelKeberangkatan.getNama_pemasokBarang());
        kedatanganViewHolder.b.setText(modelKeberangkatan.getTujuan());
        kedatanganViewHolder.c.setText(modelKeberangkatan.getTanggal());

        kedatanganViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(mContext, DetailPemasokBarang.class);

                x.putExtra("id", modelKeberangkatan.getId());
                x.putExtra("id_pemasokBarang", modelKeberangkatan.getId_pemasokBarang());
                x.putExtra("nama_pemasuk", modelKeberangkatan.getNama_pemasokBarang());
                x.putExtra("tujuan", modelKeberangkatan.getTujuan());
                x.putExtra("tanggal", modelKeberangkatan.getTanggal());

                mContext.startActivity(x);
            }
        });
    }

    @Override
    public int getItemCount() {
       return modelKeberangkatans.size();
    }

    public class KedatanganViewHolder extends RecyclerView.ViewHolder{
        TextView a,b,c;
        LinearLayout linearLayout;
        public KedatanganViewHolder(@NonNull View itemView) {
            super(itemView);

            a = (TextView) itemView.findViewById(R.id.nama_pemasokBarang);
            b = (TextView) itemView.findViewById(R.id.tujuan);
            c = (TextView) itemView.findViewById(R.id.tanggal_berangkat);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear1);
        }
    }
}
