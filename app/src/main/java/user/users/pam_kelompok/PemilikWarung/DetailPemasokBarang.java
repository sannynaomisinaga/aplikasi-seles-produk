package user.users.pam_kelompok.PemilikWarung;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import user.users.pam_kelompok.R;

public class DetailPemasokBarang extends AppCompatActivity {

    TextView a,b,c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemasok_barang);

        a = (TextView) findViewById(R.id.t_view_namaPemasok);
        b = (TextView) findViewById(R.id.t_view_tujuan);
        c = (TextView) findViewById(R.id.t_view_tanggal);

        Intent dapatkanData = getIntent();

        String nama_Pemasok = dapatkanData.getExtras().getString("nama_pemasuk");
        String tujuan = dapatkanData.getExtras().getString("tujuan");
        String tanggal = dapatkanData.getExtras().getString("tanggal");


        a.setText(nama_Pemasok);
        b.setText(tujuan);
        c.setText(tanggal);
    }
}
