package user.users.pam_kelompok.PemilikWarung;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.Adapter.KedatanganAdapter;
import user.users.pam_kelompok.Model.ModelKeberangkatan;
import user.users.pam_kelompok.PemasokBarang.Index_PBarang;
import user.users.pam_kelompok.R;

public class IndexPemilikWarung extends AppCompatActivity {

    RecyclerView myRecyclerView;
    ApiInterface apiInterface;
    KedatanganAdapter kedatanganAdapter;
    List<ModelKeberangkatan> data_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index_pemilik_warung);

        myRecyclerView = (RecyclerView) findViewById(R.id.myRcyclerView);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        data_list = new ArrayList<>();

        Intent x = getIntent();
        final int id_ = x.getExtras().getInt("id_user");

        Call<List<ModelKeberangkatan>> call = apiInterface.getAllKeberangkatan();

        call.enqueue(new Callback<List<ModelKeberangkatan>>() {
            @Override
            public void onResponse(Call<List<ModelKeberangkatan>> call, Response<List<ModelKeberangkatan>> response) {
                List<ModelKeberangkatan> list = response.body();
                ModelKeberangkatan modelKeberangkatan = null;

                for(int i=0; i< list.size();i++) {
                    modelKeberangkatan = new ModelKeberangkatan();

                    int id_keberangkatan = list.get(i).getId();
                    int id_pemasok = list.get(i).getId_pemasokBarang();
                    String nama_pemasok = list.get(i).getNama_pemasokBarang();
                    String tujuan = list.get(i).getTujuan();
                    String tanggal = list.get(i).getTanggal();

                    modelKeberangkatan.setId(id_keberangkatan);
                    modelKeberangkatan.setId_pemasokBarang(id_pemasok);
                    modelKeberangkatan.setNama_pemasokBarang(nama_pemasok);
                    modelKeberangkatan.setTujuan(tujuan);
                    modelKeberangkatan.setTanggal(tanggal);

                    data_list.add(modelKeberangkatan);
                }

                kedatanganAdapter = new KedatanganAdapter(IndexPemilikWarung.this, data_list);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(IndexPemilikWarung.this,1);


                myRecyclerView.setLayoutManager(gridLayoutManager);
                myRecyclerView.setAdapter(kedatanganAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelKeberangkatan>> call, Throwable t) {

            }
        });
    }
}
