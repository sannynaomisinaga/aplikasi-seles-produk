package user.users.pam_kelompok.PemasokBarang;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.Adapter.BarangAdapter;
import user.users.pam_kelompok.Model.ModelBarang;
import user.users.pam_kelompok.R;

public class Index_PBarang extends AppCompatActivity {

    ApiInterface apiInterface;
    RecyclerView myRecyclerView;
    BarangAdapter barangAdapter;
    List<ModelBarang> data_list;
    Button tambah_barang, buat_keberangkatan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index__pbarang);

        myRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        data_list = new ArrayList<>();
        Intent x = getIntent();
        final int id_ = x.getExtras().getInt("id_user");

        buat_keberangkatan = (Button) findViewById(R.id.btn_buatPemberitahuan);

        buat_keberangkatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x = new Intent(Index_PBarang.this, Buat_Keberangkatan.class);
                x.putExtra("id_user", id_);
                startActivity(x);
            }
        });

        tambah_barang = (Button) findViewById(R.id.btn_tambahBarang);
        tambah_barang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Index_PBarang.this,TambahBarang.class);
                a.putExtra("id_user", id_);

                startActivity(a);
            }
        });

        Call<List<ModelBarang>> call = apiInterface.getBarangPerPBarang(id_);

        call.enqueue(new Callback<List<ModelBarang>>() {
            @Override
            public void onResponse(Call<List<ModelBarang>> call, Response<List<ModelBarang>> response) {

                List<ModelBarang> list = response.body();
                ModelBarang modelBarang = null;

                for(int i=0; i < list.size(); i++ ) {

                    modelBarang = new ModelBarang();

                    int id_barang = list.get(i).getId();
                    String nama_barang = list.get(i).getNama_barang();
                    int id_pemasokBarang = list.get(i).getId_pemasokBarang();
                    Double pricee = list.get(i).getHarga_barang();
                    String nomor_barang = list.get(i).getNomor_barang();
                    String deskripsi = list.get(i).getDeskripsi_barang();
                    String status = list.get(i).getDeskripsi_barang();
                    String gambar = list.get(i).getGambar();


                    modelBarang.setId(id_barang);
                    modelBarang.setNama_barang(nama_barang);
                    modelBarang.setId_pemasokBarang(id_pemasokBarang);
                    modelBarang.setHarga_barang(pricee);
                    modelBarang.setNomor_barang(nomor_barang);
                    modelBarang.setDeskripsi_barang(deskripsi);
                    modelBarang.setStatus(status);
                    modelBarang.setGambar(gambar);

                    data_list.add(modelBarang);
                }

                barangAdapter = new BarangAdapter(Index_PBarang.this, data_list);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(Index_PBarang.this,3);

                myRecyclerView.setLayoutManager(gridLayoutManager);
                myRecyclerView.setAdapter(barangAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelBarang>> call, Throwable t) {

            }
        }) ;

    }
}
