package user.users.pam_kelompok.PemasokBarang;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.Model.ModelBarang;
import user.users.pam_kelompok.R;

public class TambahBarang extends AppCompatActivity {
    ApiInterface apiInterface;
    Button add;
    EditText input_namaBarang,input_hargaBarang,input_nomorBarang, input_deskripsiBarang, input_status;
    int id_userr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_barang);

        input_namaBarang = (EditText) findViewById(R.id.tf_nama);
        input_hargaBarang = (EditText) findViewById(R.id.tf_hargaBarang);
        input_nomorBarang = (EditText) findViewById(R.id.tf_nomorBarang);
        input_deskripsiBarang = (EditText) findViewById(R.id.tf_deskripsi_barang);
        input_status = (EditText) findViewById(R.id.tf_status);

        add = (Button) findViewById(R.id.btn_tambahBarang);

        Intent getData = getIntent();
        int idUser = getData.getExtras().getInt("id_user");

        id_userr = idUser;

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBarang();
            }
        });
    }

    public void createBarang() {
        Double Harg;
        String nama = input_namaBarang.getText().toString();
        String harga = input_hargaBarang.getText().toString();
        Harg = Double.parseDouble(harga);
        String nomor = input_nomorBarang.getText().toString();
        String deskripsi = input_deskripsiBarang.getText().toString();
        String status = input_status.getText().toString();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ModelBarang> call = apiInterface.registerBarang(id_userr,nama,Harg,nomor,deskripsi,null,status);

        call.enqueue(new Callback<ModelBarang>() {
            @Override
            public void onResponse(Call<ModelBarang> call, Response<ModelBarang> response) {
                if(response.isSuccessful()){
                    System.out.println("Berhasil Mendaftarkan Barang");
                }
            }

            @Override
            public void onFailure(Call<ModelBarang> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });

        Intent intent = new Intent(TambahBarang.this, Index_PBarang.class);
        intent.putExtra("id_user", id_userr);
        startActivity(intent) ;
    }
}
