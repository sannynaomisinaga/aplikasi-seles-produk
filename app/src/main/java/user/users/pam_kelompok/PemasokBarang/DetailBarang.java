package user.users.pam_kelompok.PemasokBarang;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.SQLOutput;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.Model.ModelBarang;
import user.users.pam_kelompok.R;

public class DetailBarang extends AppCompatActivity {

    ApiInterface apiInterface;
    EditText input_nama, input_harga,input_deskripsi,input_status;
    Button del,updt;

    int id_barang;
    int id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);

        input_nama = (EditText) findViewById(R.id.tf_nama);
        input_harga = (EditText) findViewById(R.id.tf_harga);
        input_deskripsi = (EditText) findViewById(R.id.tf_deskripsi);
        input_status = (EditText) findViewById(R.id.tf_status);

        Intent x = getIntent();
        String a = x.getExtras().getString("nama");
        double bb = x.getExtras().getDouble("harga");
        String b = String.valueOf(bb);
        String c = x.getExtras().getString("desc");
        String d = x.getExtras().getString("stat");

        id_user = x.getExtras().getInt("id_user1");

        int id__ = x.getExtras().getInt("id_user");
        id_barang = id__;

        input_nama.setText(a);
        input_harga.setText(b);
        input_deskripsi.setText(c);
        input_status.setText(d);

        System.out.println(id__);

        del = (Button) findViewById(R.id.btn_delete);
        updt = (Button) findViewById(R.id.btn_update);

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusBarang();
            }
        });

        updt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateBarang();
            }
        });
    }

    public void hapusBarang() {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiInterface.deleteBarang(id_barang);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println(response.code());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });

        Intent x = new Intent(this, Index_PBarang.class);
        x.putExtra("id_user", id_user);
        startActivity(x);
    }

    public void updateBarang() {
        Double price;
        price = Double.parseDouble(input_harga.getText().toString());
        ModelBarang modelBarang = new ModelBarang(input_nama.getText().toString(),price,input_deskripsi.getText().toString(),input_status.getText().toString());

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ModelBarang> call  = apiInterface.updateBarang(id_barang, modelBarang);

        call.enqueue(new Callback<ModelBarang>() {
            @Override
            public void onResponse(Call<ModelBarang> call, Response<ModelBarang> response) {
                if(response.isSuccessful()) {
                    System.out.println("Berhasil Mengupdate data");
                }
            }

            @Override
            public void onFailure(Call<ModelBarang> call, Throwable t) {

            }
        });

        Intent x = new Intent(this, Index_PBarang.class);
        x.putExtra("id_user", id_user);
        startActivity(x);
    }
}
