package user.users.pam_kelompok.PemasokBarang;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.users.pam_kelompok.API.ApiClient;
import user.users.pam_kelompok.API.ApiInterface;
import user.users.pam_kelompok.Model.ModelKeberangkatan;
import user.users.pam_kelompok.R;

public class Buat_Keberangkatan extends AppCompatActivity {

    ApiInterface apiInterface;
    EditText a,b;
    Button c;
    int id_userr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat__keberangkatan);

        a = (EditText) findViewById(R.id.tf_tujuan);
        b = (EditText) findViewById(R.id.tf_tanggal);

        c = (Button) findViewById(R.id.save_data);

        Intent getData = getIntent();
        id_userr = getData.getExtras().getInt("id_user");

        String xx = String.valueOf(id_userr);
        a.setText(xx);

        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createKeberangkatan();
            }
        });
    }

    public void createKeberangkatan() {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ModelKeberangkatan> call = apiInterface.tambahKeberangkatan(id_userr,a.getText().toString(),b.getText().toString());

        call.enqueue(new Callback<ModelKeberangkatan>() {
            @Override
            public void onResponse(Call<ModelKeberangkatan> call, Response<ModelKeberangkatan> response) {

            }

            @Override
            public void onFailure(Call<ModelKeberangkatan> call, Throwable t) {

            }
        });

        Intent x = new Intent(Buat_Keberangkatan.this, Index_PBarang.class);
        x.putExtra("id_user", id_userr);
        startActivity(x);
    }

}
